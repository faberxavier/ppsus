# ppsus

## Proposta 

O software desenvolvido tem como objetivo proporcionar a automação da análise dos procedimentos para controle de qualidade de gama câmaras. Desta forma, toda interpretação dos resultados deve ser realizada pelo profissional responsável. O Software foi desenvolvido em JAVA. 

## Testes Implementados

1. Sensibilidade planar ou tomográfica
1. Velocidade da mesa de exame 
1. Resolução e linearidade 
1. Uniformidade
1. Angulação
1. Radiação de fundo da sala de exame

## Utilização
A utilização do sistema é realizada exclusivamente via terminal. Sendo que os parâmetros para execução dos testes devem ser inseridos de acordo com a ordem abaixo:
* Tipo de Teste
* Path da Imagem
* Parâmetros específicos de cada teste

### Sensibilidade (n)
Expressar todos os dados como taxas de contagens líquida, ou seja, corrigido para o background. Corrigir os dados relativos a cada colimador de baixa energia para o decaimento de 99mTc a partir da medição da seringa ao tempo de contagem do fantoma( uma correção de decaimento não é necessária para o 67Ga, 111In, ou 131I).

Calcula-se a sensibilidade de cada colimador planar em contagens por segundo por becquerel, ou unidades equivalentes para coincidir com as especificações do fabricante.
Observações. Uma precisão de 5% é suficiente para indicar se as sensibilidades de diferentes
colimadores são comparáveis com as especificações do fabricante
```
Exemplo de uso:
java -jar PPSUS.jar n SENS_LEHR_F_F001_DS.dcm 17.32 
```

### Velocidade de Mesa (s ou f)
Verifique visualmente as varreduras para variações de intensidade. Desenhe o perfil longitudinal através da imagem digital. Para uma varredura de duas passagens, desenhe dois perfis longitudinais, uma através do meio de cada passagem.
Limite de Aceitação / Resultados Esperados
Se as flutuações na intensidade do contorno exceder 5%, uma ação corretiva deve ser iniciada através do representante do fabricante. Se as regiões das bordas aumentarem ou diminuírem de forma linear, na abertura ou no fechamento eletrônico, a velocidade da janela precisa de ajuste

```
Exemplo de uso (teste de régua):
java -jar PPSUS.jar s VEL_REGUA_E001_DS.dcm 17.32 7 
```

```
Exemplo de uso (teste de flood):
java -jar PPSUS.jar f VEL_FLOOD_E001_DS.dcm
```

### Resolução e Linearidade (r)
Determine as larguras das menores barras que a câmara de cintilação pode resolver nas direções X e Y. Isto pode ser feito por meio de inspeção visual das imagens. Observe todas as áreas de baixa resolução espacial, que pode corresponder à localização dos PMTs ou a borda do campo de visão.
Estime as resoluções espaciais intrínsecas nas direções X e Y em termos de FWHM da função de linha de propagação, usando a relação: FWHM = 1.75xB; onde B é a largura das menores barras que a câmara pode resolver. Calcule a média dos valores nas direções X e Y. 
```
Exemplo de uso:
java -jar PPSUS.jar r RES_LEHR_F_1_F001_DS.dcm
```
### Uniformidade (u)
Com a obtenção do número de contagens em cada pixel da matrix. Os seguintes resultados serão calculados com os pixels no UFOV e CFOV (Central Field of View - representa aproximadamente 75% do UFOV):
Valores máximo e mínimo no UFOV e CFOV 
Uniformidade total: 100 [(max - min) / max + min)] 
Valores diferenciais entre o máximo e o mínimo para um grupo de 5 pixels. 
Uniformidade diferencial: 100[(high – low)/(high + low)]
*OBS.: Caso a gama câmara tenha circuito de correção de uniformidade, o teste deverá ser feito com e sem a referida correção*
```
Exemplo de uso:
java -jar PPSUS.jar u UNIF_LEHR_E001_DS.dcm
```

### Angulação (a)
Analisar visualmente cada imagem exibida com o maior tamanho de matriz, observando a forma da  fonte pontual na imagem. A aparência da imagem no centro do colimador deve ser redonda. As imagens devem ser simétricas. Defeitos no colimador ou angulação nos furos podem aparecer como estrias ou como uma forma gravemente distorcida.
Observações: Este teste deve ser realizado também quando houver suspeita de dano a um colimador 
```
Exemplo de uso:
java -jar PPSUS.jar a UNIF_LEHR_E001_DS.dcm
```

### Radiação de fundo de sala
O valor da taxa de contagem de fundo deve ser comparado com o valor de referência e com valores recentes para identificar quaisquer alterações. Um aumento significativo da taxa de contagem de fundo pode indicar contaminação radioativa da gama câmara, dos colimadores, presença de fontes/contaminação no ambiente ou anomalia de ganho no sistema. Alternativamente pode-se indicar ruído elétrico. 

Caso o resultado se apresente anormal, o teste deverá ser repetido depois de se certificar de que não existe contaminação na gama câmara ou fontes de radiação nas proximidades. Se a contaminação for detectada, deve-se limpar o local contaminado. 
Uma inexplicável taxa de contagem de fundo deve ser monitorada por dias para ver se a taxa diminui com o decaimento radioativo ou se persiste. Uma imagem deve ser adquirida para auxiliar na determinação da origem alta taxa de contagem. A causa também pode ser uma falha elétrica.
```
Exemplo de uso:
java -jar PPSUS.jar b Bg_E001_DS.dcm
```
