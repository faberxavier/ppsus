package util.interfaces;

import util.PpsusImage;

public interface IImageTest {

    public void executeTest();

    public void setImage(PpsusImage image);

    public void setParameters(String... values);

}