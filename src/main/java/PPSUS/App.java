package PPSUS;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import tests.*;
import util.PpsusImage;
import util.Report;
import util.interfaces.IImageTest;

public class App {
    static String[] testesDisponiveis = { "a", "b", "f", "r", "s", "n", "u" };

    public static void main(String[] args) throws Exception {

        String option;

        option = args[0];
        testOption(option, args);

    }

    private static void testOption(String option, String[] parameters) throws IOException {
        if (option.equals("menu")) {
            menu();
        }

        else if (Arrays.stream(testesDisponiveis).anyMatch(option::equals)) {
            String pathImage = parameters[1];
            File tempFile = new File(pathImage);

            if (tempFile.exists()) {
                IImageTest Otest = buildTest(option, pathImage, parameters);
                Otest.executeTest();
                Report.printResult("PPSUS" + java.time.LocalDate.now() + "_RESULT");
            } else {
                System.out.println("Arquivo de imagem não existe");
            }

        } else if (option.equals("sample")) {
            sample();
        } else {
            System.out.println("Opção inválida");
            menu();
        }

    }

    private static void sample() {

        PpsusImage image = new PpsusImage("SENS_LEHR_F_F001_DS.dcm");
        Sensibility s = new Sensibility();
        s.setImage(image);
        s.setParameters("13.31");
        s.executeTest();

        image = new PpsusImage("UNIF_LEHR_E001_DS.dcm");
        Uniformity uni = new Uniformity();
        uni.setImage(image);
        uni.executeTest();

        image = new PpsusImage("Bg_E001_DS.dcm");
        Background bg = new Background();
        bg.setImage(image);
        bg.executeTest();

        image = new PpsusImage("RES_LEHR_F_1_F001_DS.dcm");
        Resolution rs = new Resolution();
        rs.setImage(image);
        rs.executeTest();

        image = new PpsusImage("VEL_FLOOD_E001_DS.dcm");
        FloodSpeed fs = new FloodSpeed();
        fs.setImage(image);
        fs.executeTest();

        image = new PpsusImage("VEL_REGUA_E001_DS.dcm");
        ScaleSpeed scale = new ScaleSpeed();
        scale.setImage(image);
        scale.setParameters("7.1;23;32;12;4;9.22;7.1;23;32;12", "5");
        scale.executeTest();

        Report.printResult("PPSUS" + java.time.LocalDate.now() + "_RESULT");

    }

    private static IImageTest buildTest(String testOption, String pathImage, String[] args) throws IOException {
        PpsusImage image = new PpsusImage(pathImage);
        IImageTest ObjReturn = null;
        switch (testOption.toLowerCase().charAt(0)) {
        case 'a':
            ObjReturn = new Angulation();
            break;
        case 'b':
            ObjReturn = new Background();
            break;
        case 'f':
            ObjReturn = new FloodSpeed();
            break;
        case 'r':
            ObjReturn = new Resolution();
            break;
        case 's':
            ObjReturn = new ScaleSpeed();
            ObjReturn.setParameters(args[2], args[3]);
            break;
        case 'n':
            ObjReturn = new Sensibility();
            ObjReturn.setParameters(args[2]);
            break;
        case 'u':
            ObjReturn = new Uniformity();
            break;
        default:
            System.out.println("Opção inválida");
            break;
        }
        if (ObjReturn != null)
            ObjReturn.setImage(image);
        return ObjReturn;
    }

    public static void menu() {
        System.out.println(
                "\nSistema de para automação dos procedimentos para controle de qualidade de gama câmaras \n\n\t"
                        + "TESTES DISPONIVEIS\nSensibilidade planar ou tomográfica (n)\nVelocidade da mesa de exame (s ou f)\n"
                        + "Resolução e linearidade (r)\nUniformidade (u) \nAngulação (a)\nRadiação de fundo da sala de exame (b)\n\n"
                        + "A utilização do sistema é realizada exclusivamente via terminal. \nSendo que os parâmetros para execução dos testes devem ser inseridos de acordo com a ordem abaixo:\n1) Tipo de Teste \n2) Path da Imagem \n3) Parâmetros específicos de cada teste\n\n"
                        + "Exemplo de uso (teste de régua):\n\n\tjava -jar PPSUS.jar s VEL_REGUA_E001_DS.dcm 17.32 7 ");

    }

}