/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import util.graph.Graph;
import util.interfaces.IImageTest;
import util.PpsusImage;
import util.Report;

import java.io.IOException;

/**
 *
 * @author Casa
 */
public class FloodSpeed implements IImageTest {

    private PpsusImage image;
    String result = "===================================================================================================================================================================================================================================================\n"
            + "RESULTADO TESTE DE FLOODSPEED\n"
            + "===================================================================================================================================================================================================================================================\n";

    public FloodSpeed() {
    }

    public void executeTest() {

        double[] values = new double[image.getHeight()];
        for (int lin = 0; lin < image.getHeight(); lin++) {
            // ADD ALL LINE VALUES
            values[lin] = image.getValue(image.getWidth() / 2, lin);
            // System.out.println(values[lin]+","+lin);
            // System.out.print(imageMatrix[lin][col]);
        }
        try {
            saveImage(values);
        } catch (IOException e) {
            e.printStackTrace();
        }
        printResult();
    }

    private void printResult() {
        Report r = Report.getInstance();
        System.out.println(result.replaceAll("=", "") + '\n');
        r.addResult(result);

    }

    private void saveImage(double[] values) throws IOException {
        Graph chart = new Graph();
        chart.lineChart(values, "Flood Profile", "Point", "Total Count");
    }

    @Override
    public void setImage(PpsusImage image) {
        this.image = image;
    }

    @Override
    public void setParameters(String... values) {
        // TODO Auto-generated method stub

    }
}
