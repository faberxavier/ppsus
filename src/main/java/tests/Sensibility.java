/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import util.PpsusImage;
import util.Report;
import util.interfaces.IImageTest;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

/**
 *
 * @author faber.henrique
 */
public class Sensibility implements IImageTest {
    private PpsusImage image;
    private double acitivity;
    String result = "===================================================================================================================================================================================================================================================\n"
            + "RESULTADO TESTE DE SENSIBILIDADE\n"
            + "===================================================================================================================================================================================================================================================\n";

    public Sensibility() {
    }

    public void executeTest() {
        image.BinarizeImage();
        double[][] imageBin = image.getBin();
        double sum = 0;
        for (double[] imageBin1 : imageBin) {
            for (int j = 0; j < imageBin1.length; j++) {
                sum = sum + imageBin1[j];
            }
        }
        addResult("CONTAGEM TOTAL =" + sum);
        double taxa = (sum / 100 / 1000);
        addResult("TAXA (Kctg/seg) = " + taxa);
        addResult("TAXA (Kcpm/mCi) =" + (taxa * 6000) / acitivity / 1000);
        teste(imageBin);
        printResult();

    }

    private void teste(double[][] yourmatrix) {
        for (int line = (int) (image.getWidth() * 0.125); line < image.getWidth() * 0.25; line++)
            for (int col = (int) (image.getHeight() * 0.5); col < image.getHeight() * 0.625; col++)
                yourmatrix[line][col] = 0;

        try {
            BufferedImage image = new BufferedImage(yourmatrix.length, yourmatrix[0].length,
                    BufferedImage.TYPE_INT_ARGB);
            for (int i = 0; i < yourmatrix.length; i++) {
                for (int j = 0; j < yourmatrix[i].length; j++) {
                    int a = (int) yourmatrix[i][j];
                    Color newColor = new Color(a, a, a);
                    image.setRGB(j, i, newColor.getRGB());
                }
            }
            File output = new File("Sensibilidade.jpg");
            ImageIO.write(image, "jpg", output);
        } catch (Exception e) {
            System.out.println(e);

        }
    }

    private void addResult(String data) {

        result = result + data + "\n";
    }

    private void printResult() {
        Report r = Report.getInstance();
        System.out.println(result.replaceAll("=", "") + '\n');
        r.addResult(result);

    }

    @Override
    public void setImage(PpsusImage image) {
        this.image = image;
    }

    @Override
    public void setParameters(String... values) {
        this.acitivity = Double.parseDouble(values[0]);
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
